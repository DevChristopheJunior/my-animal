<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Form\AnimalType;
use App\Repository\AnimalRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnimalController extends AbstractController
{
    /**
     * @Route("/", name="animaux")
     */
    public function index(AnimalRepository $repository)
    {
        $animaux = $repository->findAll();
        return $this->render('animal/index.html.twig',[
            "animaux" => $animaux

        ]);
    }

     /**
     * @Route("/animal/{id}", name="afficher_animal")
     */
    public function afficherAnimal(Animal $animal)
    {
        return $this->render('animal/afficherAnimal.html.twig',[
            "animal" => $animal
        ]);
    }

     /**
     * @Route("adminAnimal/{id}", name="ajout_animal")
     */
    public function modification(Animal $animal, Request $request, EntityManagerInterface $entityManager)
    {

        $form = $this->createForm(AnimalType::class,$animal);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($animal);
            $entityManager->flush();
            return $this->redirectToRoute("animaux");
           

        }

        return $this->render('adminAnimal/ajoutAnimal.html.twig',[
            "animal" => $animal,
            "form" => $form->createView()
        ]);
    }
}
