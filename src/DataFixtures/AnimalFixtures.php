<?php

namespace App\DataFixtures;

use App\Entity\Animal;
use App\Entity\Famille;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AnimalFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $fam1 = new Famille();
        $fam1->setLibelle("mammifères")
            ->setDescription("Animaux vertébrés nourissant leurs petits avec du lait")
            ;
        $manager->persist($fam1);

        $fam2 = new Famille();
        $fam2->setLibelle("reptiles")
            ->setDescription("Animal vertébré, généralement ovipare, à température variable, à respiration pulmonaire, à peau couverte d'écailles")
            ;
        $manager->persist($fam2);

        $fam3 = new Famille();
        $fam3->setLibelle("poissons")
            ->setDescription("animaux aquatiques : ils vivent sous l'eau, vertébrés, ils possèdent des branchies qui leur permettent de respirer sous l'eau, et nagent grâce à leurs nageoires.")
            ;
        $manager->persist($fam3);
        
        

        $aml1 = new Animal();
        $aml1->setNom("Chien")
            ->setDescription("Famille des Canidés")
            ->setFile("chien.png")
            ->setpoids(20)
            ->setDangereux(false)
            ->setFamille($fam1)
        ;
        $manager->persist($aml1);

        $aml2 = new Animal();
        $aml2->setNom("Chat")
            ->setDescription("Famille des Félidés")
            ->setFile("chat.png")
            ->setpoids(4)
            ->setDangereux(false)
            ->setFamille($fam1)
        ;
        $manager->persist($aml2);

        $aml3 = new Animal();
        $aml3->setNom("Cheval")
            ->setDescription("Famille des Equidés")
            ->setFile("cheval.png")
            ->setpoids(300)
            ->setDangereux(false)
            ->setFamille($fam1)
        ;
        $manager->persist($aml3);

        $aml4 = new Animal();
        $aml4->setNom("Furet")
            ->setDescription("Famille des Mustélidés")
            ->setFile("furet.png")
            ->setpoids(2)
            ->setDangereux(false)
            ->setFamille($fam1)
        ;
        $manager->persist($aml4);

        $aml5 = new Animal();
        $aml5->setNom("Serpent")
            ->setDescription("Famille des vipéridés")
            ->setFile("serpent.png")
            ->setpoids(5)
            ->setDangereux(true)
            ->setFamille($fam2)
        ;
        $manager->persist($aml5);

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
