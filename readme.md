# My animal

My animal est une application pour le choix d'un animal domestique.

## Environnement de développement
### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* npm

### lancer l'environnement de développement

```bash
composer install
npm install
npm run build
symfony server:start -d

```

